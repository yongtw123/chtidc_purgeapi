<?php

use Purge\Log;
use Purge\LogLevel;

require_once 'config.php';
require_once 'purge_log.php';

if (isset($_GET['url']))
{
	$urls = array($_GET['url']);
}
elseif (isset($_POST['url']))
{
	$urls = json_decode($_POST['url'], true);

	## 轉譯POST JSON物件
	if (is_null($urls)) {
		record_error("json_decode 錯誤: $_POST[url]");
		exit('false');
	}

	## 檢查POST物件 ("objects": [], or [])
	if (array_key_exists('objects', $urls)) {
		$urls = $urls['objects'];
	}
	if (!is_array($urls) ||
		!array_reduce(
			array_map('is_string', $urls),
			function($a, $b) { return $a && $b; },
			true
		)
	) {
		record_error('JSON格式錯誤');
		exit('false');
	}
}
else
{
	record_error("無提供參數");
	exit('false');
}


## 判別客戶
$path = explode('?', $_SERVER['REQUEST_URI'], 2);
$customer = trim(dirname($path[0]), "\ /");
if (array_key_exists($customer, Config::$PURGE_TARGETS))
{
	$temp = Config::$PURGE_TARGETS[$customer];
	$targethosts = (is_array($temp)) ? $temp[0] : $temp; //if given string, take as hosts; if array, take index 0
	$targetticket = (is_array($temp)) ? (bool) $temp[1] : true; //if given string, default to True; else use given value
}
else
{
	record_error("找不到 $customer 客戶對應服務主機");
	exit('false');
}


## Ticket page
$ticketid = newToken(9, true); //get unique id for ticket
$progresspage = Config::$PURGE_RESULT_DIR.$ticketid;
if ($targetticket)
{
	## 建立 ticket 資料夾
	if (!is_dir(Config::$PURGE_RESULT_DIR))
	{
		mkdir(Config::$PURGE_RESULT_DIR, 0755, true);
		$hta = fopen(Config::$PURGE_RESULT_DIR.".htaccess", "w");
		fwrite($hta, "Allow from all");
		fclose($hta);
	}

	## 寫入 ticket page
	$urlnum = count($urls);
	$ofp = fopen($progresspage,'a+');
	if ($ofp)
	{
		$return = array(
			"originalQueueLength"=>$urlnum,
			"completionTime"=>date("Y-m-d H:i:s"),
			"purgeStatus"=>"In-Progess",
			"submissionTime"=>date("Y-m-d H:i:s")
		); //format fixed!
		fwrite($ofp,json_encode($return));
		fclose($ofp);
		//chmod($progresspage, 0777);
	}
	else
	{
		record_error("Ticket page無法開啟");
		exit('false');
	}

	## 回傳至client
	$return = array(
		"return"=>"true",
		"count"=>$urlnum,
		"estimateSeconds"=>$urlnum,
		"progressUri"=>"$customer/$progresspage"
	);
}
else
{
	## 回傳至client
	$return = 'true';
}


## Delegate PURGE
if (Config::$PURGE_ASYNC)
{
	ob_end_clean();
	//ignore_user_abort(true); // optional
	ob_start();
	header("Connection: close\r\n");
	//header("Content-Encoding: none\r\n"); // Enable if gzip on httpd is enabled
	echo json_encode($return);
	header("Content-Length: ".ob_get_length());
	ob_end_flush();     // Strange behaviour, will not work
	flush();            // Unless both are called !
	//ob_end_clean();

	//sleep(5);
	require_once 'purge_exec.php';
	if ($targetticket) {
		# default
		runpurge($urls, $ticketid, $targethosts); //don't time
	}
	else {
		runpurge($urls, $ticketid, $targethosts, false, false); // don't time, don't write ticket
	}
}
else
{
	echo json_encode($return);
	$cmd = join(" ", array(
		"php purge_exec.php",
		"-u ".escapeshellarg(json_encode($urls)),
		"-t ".$ticketid,
		"-h ".$targethosts,
		"--ticket",
		"> /dev/null &"
	));
	exec($cmd);
}

/**
 * <http://php.net/manual/en/function.base64-encode.php#116348>
 * Generates a random URL-safe base64 string.
 *
 * See RFC 3548 for the definition of URL-safe base64.
 * If $n is not specified, Secure::RANDOM_LENGTH is assumed. It may be larger in future.
 *
 * @param int $n Specifies the length of the random string
 * @param bool $padding
 * @return string The result may contain A-Z, a-z, 0-9, "-" and "_". "=" is also used if $padding is true.
 */
function newToken($n, $padding = false)
{
	// Generate a new unique token
	if (!function_exists('openssl_random_pseudo_bytes')) {
		// Generate a random pseudo bytes token if openssl_random_pseudo_bytes is available
		// This is more secure than uniqid, because uniqid relies on microtime, which is predictable
		$s = pack('a*', openssl_random_pseudo_bytes($n));
		$s = str_replace(array("\n", "\r", "\n\r"), '', $s);
	} else {
		// Otherwise, fall back to a hashed uniqid
		$s = substr(hash('sha256', uniqid(null, true)), 0, $n);
	}

	return $padding ? strtr(base64_encode($s), '+/', '-_') : rtrim(strtr(base64_encode($s), '+/', '-_'), '=');
}

function record_error($msg)
{
	$e = Log::getInstance(LogLevel::SYSTEM);
	$e->record($msg);
	$e->commit();
}

?>
