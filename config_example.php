<?php

//=============================================================
// 這是 config.php 範例檔
// 請勿修改變數名稱!
// 填寫 hosts 時請
// 1. 確認可 sudo passwordless ssh 遠端主機, OR
// 2. 確認 hostname 可對應至 IP (/etc/hosts)
//=============================================================

class Config
{
	# 客戶對應主機與設定 (AP hosts, 是否寫 ticket)
	static $PURGE_TARGETS = array(
		'customer01' => 'host01 host02 host03',
		'customer02' => array('host10 host11', false),
		'customer03' => array('host30 host31 192.168.99.99', false)
	);

	# 回傳結果JSON存放位置
	static $PURGE_RESULT_DIR = "purge/";

	### ==================================
	### 選擇 logging 方式
	### ==================================
	# file: 指定檔案位置 (注意權限)
	# syslog: 使用 rsyslogd (請自行設定)
	static $PURGE_LOG_DEST = 'SYSLOG';

	# ----- PURGE_LOG_DEST = FILE -----
	# 紀錄檔log存放位置
	static $PURGE_LOGOK = 'log/purge.log';
	static $PURGE_LOGFAIL = 'log/purge.log';
	static $PURGE_LOGERROR = 'log/purge.log';
	static $PURGE_LOGSYSTEM = 'log/purge-error.log';

	# ----- PURGE_LOG_DEST = RSYSLOG -----
	static $PURGE_LOG_FACILITY = 'local2';

	### ==================================
	### PURGE 策略
	### ==================================
	# URL: ssh入主機直接利用url
	# HOST: ssh入主機對127.0.0.1進行purge並url path部分放在host header
	# SERVER: 對主機發送purge並url path部分放在host header
	# SERVER_ASYNC: 同 'SERVER' 但使用 curl_multi
	static $PURGE_STRATEGY = 'URL';

	# ----- PURGE_STRATEGY = URL | HOST -----
	# 暫存檔(pssh)存放位置
	static $PURGE_CACHE_DIR = "cache/";
	# 每次purge刪除暫存
	static $PURGE_DELETE_CACHE = false;

	### ==================================
	### DEV 功能
	### ==================================

	# (DEV) 使用function call非exec
	static $PURGE_ASYNC = false;

	# (DEV) log 立即寫入
	static $PURGE_LOG_WRITE_IMMEDIATE = true;

}

?>
