<?php

use Purge\Log;
use Purge\LogLevel;

/* 這支程式須由purge.php呼叫帶入變數,
   php purge_exec.php [array of URLs as string] [ticket id] [progress page prefix]
   範例:
   php purge_exec.php '["http://media.etmall.com.tw/ProductImage/1130690/1130690.jpg","http://media.etmall.com.tw/ProductImage/1130690/1130690-1_L.jpg","http://media.etmall.com.tw/ProductImage/1130690/1130690-1_XL.jpg"]' 23ae9f9347fa7b19fc73 /dir/of/page/
 */

$opts = getopt("u:t:h:", array('help', 'time', 'ticket'));
if ($opts !== false && count($opts) > 0) //getopt return FALSE on failure
{
	if (array_key_exists('help', $opts))
	{
		echo join("\n", array(
			'purge_exec.php 根據參數進行 PURGE 動作',
			' -u: URL, -u \'["url01", "url02", ...]\'',
			' -t: Ticket ID, 本次 PURGE 動作編號',
			' -h: Hosts, -h "host01 host02 host03"',
			' --help: 顯示此說明',
			' --time: 執行計時',
			' --ticket: 寫入 ticket page',
			''
		));
		exit;
	}
	runpurge(
		array_key_exists('u', $opts) ? json_decode($opts['u']) : '', // convert back to array...
		array_key_exists('t', $opts) ? $opts['t'] : '',
		//array_key_exists('h', $opts) ? "'$opts[h]'" : '', // pssh -H expects a quoted string...
		array_key_exists('h', $opts) ? $opts['h'] : '',
		array_key_exists('time', $opts) ? true : false,
		array_key_exists('ticket', $opts) ? true : false
	);
}

/* PURGE主函式
   $urls - Array: array of strings/URLs
   $ticketid - String: ticket id number
   $hosts - String: space-delimited string of hosts
   $time - Boolean: show duration
   $ticket - Boolean: write to ticket page
 */
function runpurge($urls, $ticketid, $hosts, $time=false, $ticket=true)
{
	require_once 'config.php'; //Configurations
	require_once 'purge_log.php'; //Log class

	$log = Log::getInstance(LogLevel::SYSTEM, $ticketid);

	switch(Config::$PURGE_STRATEGY)
	{
		case 'URL':
		case 'HOST':
			require_once 'purge_strategy_pssh.php';

			//PSSH stores stdout and stderr on disk
			$stdoutdir = rtrim(Config::$PURGE_CACHE_DIR, "/")."/$ticketid/stdout/";
			$stderrdir = rtrim(Config::$PURGE_CACHE_DIR, "/")."/$ticketid/stderr/";
			if (!mkdir($stdoutdir, 0755, true) or !mkdir($stderrdir, 0755, true)) {
				$log->record('無法建立暫存資料夾');
				exit("false");
			}

			$p = new PurgePSSH(Config::$PURGE_STRATEGY, $ticketid);
			$p->setSources($urls, $hosts)->setOutDirs($stdoutdir, $stderrdir)->setDebugOpts($time, $ticket)->run();
			break;
		case 'SERVER':
		case 'SERVER_ASYNC':
			require_once 'purge_strategy_curlm.php';

			$p = new PurgeCURLM(Config::$PURGE_STRATEGY, $ticketid);
			$p->setSources($urls, $hosts)->setDebugOpts($time, $ticket)->run();
			break;
		default:
			$log->record("無效之purge指令建立策略: $strategy, 請檢察Config");
			exit("false");
	}

	$log->commit();

}



?>
