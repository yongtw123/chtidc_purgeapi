<?php

namespace Purge;

use \Config;
use \SplQueue;

require_once 'config.php';

class Log
{
	public static function getInstance($status, $ticketid=NULL)
	{
		if (Config::$PURGE_LOG_DEST === 'FILE') {
			return new LogFile($status, $ticketid);
		}
		elseif (Config::$PURGE_LOG_DEST === 'SYSLOG') {
			return new LogSyslog($status, $ticketid);
		}
		else {
			die('Bad logging configuration.'.PHP_EOL);
		}
	}
}

class LogLevel
{
	const 	OK = 0,
			FAIL = 1,
			ERROR = 2,
			SYSTEM = -1;
}

//REVIEW Separate Log implementations another file?
abstract class BaseLog
{
	protected $store;
	protected $ticketid;

	public function __construct($ticketid)
	{
		$this->store = new SplQueue();
		$this->set_ticketid($ticketid);
	}

	public function __destruct()
	{
		$this->commit();
	}

	//store raw data to temp store
	public function write($data)
	{
		if (!empty($data)) {
			if (is_array($data)) {
				foreach($data as $d) { $this->store->enqueue($d); }
			}
			else {
				$this->store->enqueue($data); //assuming $data is well-behaved string
			}
		}
		if (Config::$PURGE_LOG_WRITE_IMMEDIATE) {
			$this->commit();
		}
	}

	//record line to temp store; msgarr can store array of messages, ex.error msg, host=status_code, ...
	public function record($msg, $url='-')
	{
		if (isset($msg)) {
			//defined and NOT null
			if (is_string($msg) && $msg !== '') {
				$dat = "\"$url\" $msg";
			}
			elseif (is_array($msg) && count($msg) > 0) {
				$dat = "\"$url\" ".join(',', $msg);
			}
			else {
				return;
			}
			$this->write( $this->postprocess($dat) );
		}
	}

	private function set_ticketid($ticketid)
	{
		$this->ticketid = (!is_null($ticketid)) ? $ticketid : '__PROGRAM__';
	}

	abstract public function commit();
	abstract protected function postprocess($msg);
}

class LogFile extends BaseLog
{
	public function __construct($status, $ticketid)
	{
		parent::__construct($ticketid);
		switch ($status)
		{
			case LogLevel::OK:
				$this->logpath = Config::$PURGE_LOGOK;
				$this->prefix = "[okay]";
				break;
			case LogLevel::FAIL:
				$this->logpath = Config::$PURGE_LOGFAIL;
				$this->prefix = "[fail]";
				break;
			case LogLevel::ERROR:
				$this->logpath = Config::$PURGE_LOGERROR;
				$this->prefix = "[error]";
				break;
			default:
				//LogLevel::SYSTEM
				$this->logpath = Config::$PURGE_LOGSYSTEM;
				$this->prefix = "[crit]";
		}
		$this->commit(); //try out stream, will die if failure
	}

	public function commit()
	{
		//NOTE Open file only when committing to avoid exhausting file handlers?
		//if path contains directories, check if directories exist
		if (!is_dir(dirname($this->logpath))) {
			mkdir(dirname($this->logpath), 0755, true);
		}

		$fp = fopen($this->logpath, "a+"); //append, atomic fwrite if under system block size
		if ($fp) {
			while(!$this->store->isEmpty())
			{
				fwrite($fp, $this->store->dequeue());
			}
			fclose($fp);
		}
		else {
			die("Cannot open log file $this->logpath".PHP_EOL);
		}
	}

	protected function postprocess($msg)
	{
		return '['.date('Y/m/d H:i:s')."] $this->prefix $this->ticketid: $msg".PHP_EOL;
	}
}

class LogSyslog extends BaseLog
{
	public function __construct($status, $ticketid)
	{
		parent::__construct($ticketid);
		switch ($status)
		{
			case LogLevel::OK:
				$this->level = LOG_INFO;
				break;
			case LogLevel::FAIL:
				$this->level = LOG_NOTICE;
				break;
			case LogLevel::ERROR:
				$this->level = LOG_ERR;
				break;
			default:
				//LogLevel::SYSTEM
				$this->level = LOG_CRIT;
		}
		$this->commit(); //try out stream, will die if failure
	}

	public function commit()
	{
		$ch = openlog($this->ticketid, LOG_ODELAY | LOG_CONS, constant('LOG_'.strtoupper(Config::$PURGE_LOG_FACILITY)));
		if ($ch) { //TRUE on success, FALSE on failure
			while(!$this->store->isEmpty())
			{
				$line = $this->store->dequeue();
				$line = trim($line); //trim unnecessary whitespace chars
				if ($line === '') { continue; }
				else { syslog($this->level, $line); }
				//NOTE Timestamp delay if not sent to Rsyslog immediately!
			}
			closelog();
		}
		else {
			syslog(LOG_ALERT, "PurgeAPI failed to open syslog stream.");
			die("Failed to open log stream.".PHP_EOL);
		}
	}

	protected function postprocess($msg)
	{
		return $msg; //no-op
	}
}

?>
