# 下載平台 PURGE API #

透過`pssh`串接多個`curl`指令 或php `multi_curl`達到PURGE requests之**平行與批次**處理

## TODO ##
+ upgrade php => curlm private opt bug fixed => revisit **curlm strategy**
+ input validation (valid http url) should be done FIRST!!!
+ cache-analayze: bad url will repeat in error log for *host* times => causing rsyslog **RATE LIMITING**!!!
+ lint config before running!
+ ***MEMORY LEAK?!***
+ how to host with HTTPS?
+ how to do complete unit test?

## 原理 ##

+ 若設定 AP 只允許 VIP 之 PURGE request， 則必須 ssh 入 AP 發送 PURGE request URL，
  因 AP 本身 loopback interface 自帶 VIP， DNS resolve 得到 IP 後會導向自己，
  也因此能保證 PURGE request 會在所有 AP 上生效而不會因 SLB 分散。
  `pssh` 目的為同時開啟多條 ssh 連線至所有 AP，並將 curl 指令合併批次處理。
+ ssh 耗時耗資源，另一方法為修改 HTTP header 內之 'Host' 欄位，如此無論 URL 前面的 hostname 為何，
  伺服器將會以 'Host' 欄位決定將 request 分至哪一個 virtual server 處理。
  於是 hostname 可以換為 AP 之 IP，然後直接發送 HTTP request。
  此部分由 PHP 內建 curl 與 multi_curl 進行，達到平行與批次處理。

## 安裝 ##

1. `yum install pssh curl`
2. `yum update php`
3. `cd [資料夾]; cp config_example.php config.php`
4. 修改 `config.php` (`$PURGE_TARGETS` 必修 => 可對應 httpd `<LOCATION>` directive)
5. (Optional) httpd 開放
    * `./setup.sh` (將必要檔案 copy 至 `/var/www/html/purgeapi` 並修改資料夾權限)
    * 視需求設定 httpd (使用 httpd `<LOCATION>` directive)
    * **警告** 若 php 有開 Xdebug 必須將其關閉否則會大幅影響程式效率，嚴重則系統過載
6. (Optional) Rsyslog 設定
    * 若 config 指定使用 `SYSLOG` 紀錄
7. (Optional) Unit test
    * `wget https://phar.phpunit.de/phpunit-old.phar`
    * `chmod +x phpunit-old.phar`
    * `mv phpunit-old.phar /usr/local/bin/phpunit`
    * 若鍵入 `phpunit` 找不到，確認 bash hash 是否指錯路徑

        type phpunit
        hash -d phpunit
        phpunit

    * 測試 `phpunit` 可否執行， 不然不進行 unit test

## 設定 ##

於`config.php`修改重要參數

+ `$PURGE_TARGETS`: 指定客戶 purge 對象主機 及 是否需要寫 ticket
+ `$PURGE_RESULT_DIR`: 指定 purge 結果 JSON 寫入位置
+ `$PURGE_LOG_DEST`: 指定 log 方式
+ `$PURGE_LOG`\*: 指定 log 檔案位置 (若 log 使用 FILE 輸出)
+ `$PURGE_LOG_FACILITY`: 指定 log 使用 syslog facility (若 log 使用 SYSLOG 輸出)
+ `$PURGE_STRATEGY`: 指定使用 PURGE 運行模式
+ `$PURGE_CACHE_DIR`: 指定 purge 暫存 (pssh 回傳結果) 寫入位置
+ `$PURGE_DELETE_CACHE`: 每次 purge 後馬上刪除 pssh 暫存
+ `$PURGE_ASYNC`: (進階) 使用 function 呼叫 (可透過 APC 機制得到加速)
+ `$PURGE_LOG_WRITE_IMMEDIATE`: (進階) 立即寫入 log 可保持順序

### PURGE 策略解釋
+ URL: `ssh` 進入 AP 主機並利用原網址發送 PURGE request，
  **nginx conf 必須允許從 VIP 來的 PURGE request**
+ HOST: `ssh` 進入 AP 主機，將原網址 domain 替換為 127.0.0.1 並將 domain 放入 `Host` header
  再發送之，好處是不用每一個 conf 都要修改允許 PURGE 來源之 IP (i.e. VIP)，
  **nginx conf 必須允許從 127.0.0.1 來的 PURGE request**
+ SERVER: 將原網址 domain 替換為各 AP 主機 IP 並將 domain 放入 `Host` header 再發送之，
  此為直接 HTTP request 無須 `ssh` 故速度應最快，
  **nginx conf 必須允許發出 PURGE request 跳板主機之IP**
+ SERVER_ASYNC: 同 SERVER 策略，但內部使用 `curl_multi` 函數進行平行處理以加速批次運行

結論而言：nginx conf `proxy_cache_purge` 允許 IP 建議為 VIP, 127.0.0.1, PURGE 跳板 IP

### (Optional) HTTPD 設定 ###
+ `./setup.sh` 會將必要檔案 copy 至 `/var/www/html/purgeapi/`
+ 因 HTTP 端口 (`purge.php`) 會根據 URL domain 的 path 判斷客戶，因此 httpd conf 需使用 `<LOCATION>` 區別，Alias 必須與 `$PURGE_TARGETS` 同步。

        Alias /customer01 /var/www/html/purgeapi
        <Location /customer01>
            AuthName "Restrict access"
            AuthType Basic
            AuthUserFile ...
            Require user ...
        </Location>
        <Directory /var/www/html/purgeapi>
            AllowOverride Limit
            order deny, allow
            deny from all
            allow from all
        </Directory>

+ `/var/www/html/purgeapi` 自帶 `.htaccess` 會限制存取， 除了 `purge.php` 及 `purgegui.php` 以外皆禁止
+ 程式運行中會自動產生 `purge` 資料夾，此資料夾將自帶 `.htaccess` *接受* 存取

### (Optional) Rsyslog 設定 ###
+ 使用之 facility 必須與 `config.php` 內 `$PURGE_LOG_FACILITY` 同步

        //rsyslog.conf
        ...;local2.none     /var/log/messages
        //不要丟到messages裡面

        //rsyslog.d/purgeapi.conf
        $template PURGELOG_FORMAT,"[%TIMESTAMP%] [%syslogseverity-text%] %syslogtag%%msg%\r\n"
        local2.err                 /var/log/purgeapi/error.log;PURGELOG_FORMAT
        local2.debug,local2.!err   /var/log/purgeapi/purge.log;PURGELOG_FORMAT
        //err以上往error.log丟， err以下(不含)往purge.log丟

+ `service rsyslog restart`
+ 設定 Logrotate
    - 新增 conf 於 `/etc/logrotate.d/`
    - 內容可參照 httpd 之 Logrotate conf

## 使用方式 ##

* 指令執行 `purge_exec.php`

        php purge_exec.php --help
        php purge_exec.php -u '["<url>", "<url>", ...]' -t <ticket_id> -h '<host> <host> ...'

* HTTP GET 至 `purge.php`，格式如下

        http://.../purge.php?url=http://domain/to/purge

* HTTP POST 至 `purge.php`，POST 內容須為以下二格式其中一種

        url={"objects": ["<url:string>", "<url:string>", ...]}
        url=["<url:string>", "<url:string>", ...]

* `cd` 入程式資料夾鍵入 `phpunit `進行 unit test

## 維運 ##

程式運行中會產生四種級別 log，分別為如下:

1. OK: 紀錄 purge 成功 (HTTP 200) 之連結
    - 使用 `SYSLOG` 模式會顯示 tag `[info]`
2. FAIL: 紀錄 purge 失敗 (Non HTTP 200) 之連結、紀錄失敗主機與 HTTP 碼
    - 通常 404 表示伺服器在 cache 找不到檔案，請 GET 一次後再試
    - 使用 `SYSLOG` 模式會顯示 tag `[notice]`
3. ERROR: purge 錯誤
    - 使用 `SYSLOG` 模式會顯示 tag `[error]`
4. SYSYTEM: 記錄錯誤訊息及其他嚴重錯誤
    - 使用 `SYSLOG` 模式會顯示 tag `[crit]`

## 程式架構 ##

* `purge.php` (web HTTP POST 進入口)
    + 根據 URL 選擇使用 config 內設定之客戶對應主機
    + 取得 POST object， 擷取矩陣部分
    + generate ticket id， 回傳 response JSON
    + delegate PURGE -> `purge_exec.php`， 根據 `$PURGE_ASYNC`:
        - 使用 `runpurge()` 函式 (可享有 APC cache 機制)， 或
        - 使用 `exec()` 呼叫並非透過 APACHE 因此無法享有 APC cache 機制 = 每次 request 都要重新 compile)
* `purge_exec.php` (主程式， 可透過command line呼叫)
    + 根據選擇之模式，使用 PSSH 或 PHP CURL
        - PSSH: 串接`curl` requests， 一次`pssh`丟給AP執行
        - PHP CURL: 開啟多個 curl handler， 一次`multi_curl`連線至AP執行
    + delegate 回傳結果處理 -> `purge_cacheanalyze.php`
    + 若 `$PURGE_DELETE_CACHE=true` 刪除回傳結果暫存
* `purge_cacheanalyze.php` (平行運算回傳結果處理 模組)
    + 讀取， 驗證， 反轉結構 (host - url - status_code => url - host - status_code)
    + 每個 URL， generate 3 lists {Ok， Fail， Error}， 每一個 list 紀錄有哪些 host
    + 若 logging 開啟， 根據以上 lists 和反轉結構寫入個別 log
    + 每個 URL， 根據以上 lists 判斷結果狀態 (假如 error list 有host => Error， 不然假如 fail list 有 host => Fail， 不然假如 ok list 沒有缺 => Ok)
    + 判斷整批的狀態 (邏輯類上)
* `purge_log.php` (平行運算回傳結果寫入 log 模組)
    + 採 SplQueue `enqueue`/`dequeue`
    + 若 `$PURGE_LOG_WRITE_IMMEDIATE=true` 啟動立即寫入 (同樣 URL 會緊鄰在一起)
    + append mode 為 atomic 因此應不需擔心 interlaced output

## pssh 回傳結果補充說明 ##
```php
$execstr .= "printf '".$index." ';";
$execstr .= "curl -X PURGE -s -S -D - -o /dev/null ".$file_url." | grep HTTP | cut -d ' ' -f 2 | tr -d ' \n\r\t';";
$execstr .= "echo '';"; //force newline
```
`pssh`回傳之結果應有 stdout 與 stderr 資料夾，每個資料夾有對應所有 hosts 名稱的檔案

根據程式碼，檔案內容應如下:

```
0 200
1 404
2
3 200
...
```

第一欄為 URL 編號(不用全 URL 以降低傳輸資料量)，第二欄為grep出來之 HTTP status code，若空則代表`curl`指令應出錯了，需檢查 stderr

## 其他故障排除

1. 持續回傳 403
    - 檢查 URL 之 domain 與 AP 端設定是否吻合 (如果不吻合極有可能跑到 access.log 下)
    - 檢查 AP config 之 `proxy_cache_purge` 是否允許 **正確** 的 IP (注意可能是 public 或 private IP...)
    - 續上，確定是否使用正確的 PURGE 策略
2. 使用 FILE log 或 cache dir，`/var/log/httpd/error_log` 持續出現 permission denied
    - 是否有用 symlink? `config.php` 內定義之相對路徑是以存取檔案之路徑為基準  
      因此若 symlink 是在別的地方，於該地建立新資料夾可能權限不足失敗
    - `config.php` 嘗試更改路徑
    - 檢查 SELinux!!
3. 找不到客戶
    - 檢查 `config.php` 和客戶所使用之連結
    - 若客戶直接連線至 web root，`$PURGE_TARGETS`只指定一個，key 為空字串 `''`
