<?php

require_once 'purge_strategy.php';
require_once 'purge_cacheanalyze.php';
require_once 'config.php';

class PurgePSSH extends Purge
{
	private $command;

	public function generate()
	{
		if ($this->strategy === 'URL') {
			$this->urlstrategy();
		}
		elseif ($this->strategy === 'HOST') {
			$this->hoststrategy();
		}
		else {
			$this->syserr->record("無效之purge指令建立策略: $this->strategy");
		}
	}

	public function execute()
	{
		/**
		 * 若通過apache執行 以下ssh會失敗 (password-less只有root)
		 * 因此必須拉高至root執行以下動作
		 */
		$hosts = (is_array($this->hosts)) ? join(' ', $this->hosts) : $this->hosts; //just in case if forgot hosts is Array
		shell_exec("/usr/bin/sudo /usr/bin/pssh -H '$hosts' -o $this->outdir -e $this->errdir \"$this->command\"");
	}

	public function results()
	{
		$analysis = new PurgeAnalysis($this->ticketid, $this->urls, $this->hosts);
		$result_status = $analysis->parse($this->outdir, $this->errdir, true, true);

		//寫入給客戶之結果頁面
		if ($this->to_write_ticket) {
			parent::writeTicket(
				Config::$PURGE_RESULT_DIR.$this->ticketid,
				count($this->urls),
				$result_status,
				$this->start_time
			);
		}

		//刪除暫存
		//NOTE Consider writing PSSH cache to /tmp
		if (Config::$PURGE_DELETE_CACHE) {
			parent::delTree(Config::$PURGE_CACHE_DIR.$this->ticketid);
		}
	}

	private function urlstrategy()
	{
		$execstr = "";
		foreach($this->urls as $index=>$file_url)
		{
			$result = $this->touchURL($file_url);
			if (empty($result))
			{
				$this->syserr->record("無效之URL: $file_url, 跳過");
				continue;
			}

			/* VERSION A: using curl verbose output => too difficult to parse output */
			//$execstr .= "echo '".$delim."';";
			//$execstr .= "curl -X PURGE -s -v -o /dev/null ".$file_url." 2>&1 | grep 'HTTP\|Host:\|* ';";
			/*
			   -s: no progress bar; silent
			   -o: redirect response body to /dev/null
			   -v: verbose, output request and response headers
			   2>&1: verbose output is sent to stderr so redirect it to stdout
			   |grep 'HTTP\|Host': getting the request line, Host, return status code, and any other info lines (may be errors)

			   Sample output:
			   * About to connect() to media.etmall.com.tw port 80 (#0)
			   *   Trying 203.66.159.2... connected
			   * Connected to media.etmall.com.tw (203.66.159.2) port 80 (#0)
			   > PURGE /ProductImage/1130690/1130690.jpg HTTP/1.1
			   > Host: media.etmall.com.tw
			   < HTTP/1.1 403 Forbidden
			   * Connection #0 to host media.etmall.com.tw left intact
			   * Closing connection #0
			 */

			/* VERSION B: echo url so only need curl status code */
			$execstr .= "printf '$index ';";
			$execstr .= "curl -X PURGE -s -S -D - -o /dev/null ";
			$execstr .= escapeshellarg($result[uri]);
			$execstr .= ($result['ishttps']) ? " -k" : "";
			$execstr .= " | grep HTTP | cut -d ' ' -f 2 | tr -d ' \n\r\t';";
			$execstr .= "echo '';"; //force newline
			/*
			   -S: show error (outputs to stderr)
			   -D -: dump header to stdout
			   tr -d ' \n\r\t': remove any whitespace char
			   cut -d ' ' -f 2: cut line ex. HTTP/1.1 200 OK using ' ' as delim and output 2nd column
			*/
		}
		$this->command = $execstr;
	}

	private function hoststrategy()
	{
		$execstr = "";
		foreach($this->urls as $index=>$file_url)
		{
			$result = parent::touchURL($file_url, '127.0.0.1');
			if (empty($result))
			{
				$this->syserr->record("無效之URL: $file_url, 跳過");
				continue;
			}

			$execstr .= "printf '$index ';";
			$execstr .= "curl -X PURGE -s -S -D - -o /dev/null $result[uri] --header 'Host: $result[host]'";
			$execstr .= ($result['ishttps']) ? " -k" : "";
			$execstr .= " | grep HTTP | cut -d ' ' -f 2 | tr -d ' \n\r\t';";
			$execstr .= "echo '';"; //force newline
		}
		$this->command = $execstr;
	}

}

?>
