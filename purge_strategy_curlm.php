<?php

require_once 'purge_strategy.php';
require_once 'purge_cacheanalyze.php';
require_once 'config.php';

class PurgeCURLM extends Purge
{
	private $handles, $responses;

	public function generate()
	{
		$this->serverstrategy();
	}

	public function execute()
	{
		if ($this->strategy === 'SERVER') {
			$this->execute_curl();
		}
		elseif ($this->strategy === 'SERVER_ASYNC') {
			$this->execute_curl_multi();
		}
		else {
			$this->syserr->record("無效之purge指令建立策略: $this->strategy");
		}
	}

	public function results()
	{
		//stderr has been treated already
		$analysis = new PurgeAnalysis($this->ticketid, $this->urls, $this->hosts);
		$result_status = $analysis->run($this->responses);

		//寫入給客戶之結果頁面
		if ($this->to_write_ticket) {
			parent::writeTicket(
				Config::$PURGE_RESULT_DIR.$this->ticketid,
				count($this->urls),
				$result_status,
				$this->start_time
			);
		}
	}

	private function serverstrategy()
	{
		$curls = array();
		foreach($this->hosts as $host)
		{
			$curls[$host] = array();
			foreach($this->urls as $index=>$file_url)
			{
				$result = $this->touchURL($file_url, $host);
				if (empty($result))
				{
					$this->syserr->record("無效之URL: $file_url, 跳過");
					#TODO Bad URL alert will repeat for $host times
					continue;
				}

				$h = curl_init($result['uri']);
				#TODO Make CURLOPT configurable?
				curl_setopt_array($h, array(
					CURLOPT_CUSTOMREQUEST => 'PURGE',
					CURLOPT_RETURNTRANSFER => true,
					//CURLOPT_HEADER => true,
					CURLOPT_HTTPHEADER => array("Host: $result[host]"),
					CURLOPT_TIMEOUT => 8,
					CURLOPT_CONNECTTIMEOUT => 2,
					//CURLOPT_PRIVATE => $index //url id //greatly bugged...
				));
				if ($result['ishttps'])
				{
					curl_setopt($h, CURLOPT_SSL_VERIFYPEER, false);
				}

				$curls[$host][$index] = $h;
			}
		}
		$this->handles = $curls;
	}

	private function execute_curl()
	{
		foreach ($this->handles as $host=>$curl_handles)
		{
			foreach ($curl_handles as $url_index=>$h)
			{
				$r = curl_exec($h);
				$this->dump_curl_info($h, $host, $url_index);
				curl_close($h); //close curl handle!
			}
		}
	}

	private function execute_curl_multi()
	{
		### ===== Build the multi handler ===== ###
		$mh = curl_multi_init();
		foreach($this->handles as $curl_handles)
		{
			foreach($curl_handles as $h)
			{
				curl_multi_add_handle($mh, $h);
			}
		}

		### ===== Execute Handles ===== ###
		$running = NULL;

		//fires execution for the first time
		do {
			$mrc = curl_multi_exec($mh, $running); //this immediately returns
		} while ($mrc === CURLM_CALL_MULTI_PERFORM); //legacy: more stuff to send, keep calling curl_multi_exec

		//are you done yet? (watch out for CPU busy-wait)
		while ($running && $mrc === CURLM_OK) {
			$dscp = curl_multi_select($mh, 2.0); //I give you max 2 seconds
			//we have select failure
			if ($dscp === -1) {
				/* BUG <https://bugs.php.net/bug.php?id=63411>
				   - curl_multi_select may return -1 forever until calling curl_multi_exec => do not use `else`
				   - curl_multi_select may return -1, it is suggested to wait some time
				   - curl_multi_select may not respect timeout after PHP version 5.3.18(?), i.e. return immediately
				 */
				$this->syserr->record('注意: curl_multi_select 回傳 -1');
				usleep(10000); //in microseconds, 1 second = 1,000 ms = 1,000,000 us
			}
			//we have activity (>= 1) or timeout (0), check again
			do {
				$mrc = curl_multi_exec($mh, $running);
			} while ($mrc === CURLM_CALL_MULTI_PERFORM);
		}

		if ($mrc !== CURLM_OK) {
			//NOTE curl_multi_strerror available *ONLY* PHP >= 5.5.0
			$merr = (function_exists('curl_multi_strerror')) ? curl_multi_strerror($mrc) : "CURLM ERROR $mrc";
			$this->syserr->record($merr);
		}

		### ===== Close the multi handler ===== ###
		foreach($this->handles as $host=>$curl_handles)
		{
			foreach($curl_handles as $url_index=>$h)
			{
				$this->dump_curl_info($h, $host, $url_index);
				curl_multi_remove_handle($mh, $h);
				curl_close($h);
			}
		}
		curl_multi_close($mh);
	}

	/**
	 * Procedure to dump related stderr/stdout from curl handler
	 */
	private function dump_curl_info($handle, $host, $url_index)
	{
		$sc = curl_getinfo($handle, CURLINFO_HTTP_CODE); //NOTE CURLINFO_HTTP_CODE returns int
		if (!$sc) {
			if ($sc === 0) {
				$this->syserr->record("$host HTTP code 0", $this->urls[$url_index]);
			}
			else if ($sc === false) {
				$this->syserr->record("$host curl_getinfo failed!", $this->urls[$url_index]);
			}
			else {
				// getinfo working, any other falsy values...
				$this->syserr->record("$host curl_getinfo dump: ".serialize(curl_getinfo($handle)),
				       $this->urls[$url_index]);
			}

			$err = curl_error($handle);
			if (!$err) {
				$this->syserr->record("$host curl_error dump: $err", $this->urls[$url_index]);
			}

			$response = curl_multi_getcontent($handle);
			if (!$response) {
				$this->syserr->record("$host content dump: ".serialize($response), $this->urls[$url_index]);
			}
		}

		//failed operations (empty result) will be detected in cacheanalyze
		$this->responses[$host][$url_index] = $sc;
	}

}


?>
