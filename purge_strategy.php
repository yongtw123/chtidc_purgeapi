<?php

use Purge\Log;
use Purge\LogLevel;

require_once 'purge_log.php';

abstract class Purge
{
	public function __construct($strategy, $ticketid)
	{
		$this->strategy = $strategy;
		$this->ticketid = $ticketid;
		$this->start_time = date("Y-m-d H:i:s");

		$this->syserr = Log::getInstance(LogLevel::SYSTEM, $this->ticketid);

		//default debug options
		$this->to_time = false;
		$this->to_write_ticket = true;
	}

	/**
	 * Set required variables
	 */
	public function setSources($urls, $hosts)
	{
		$this->urls = $urls;
		$this->hosts = explode(' ', $hosts);
		return $this;
	}

	/**
	 * Set optinal variables for PSSH mode
	 */
	public function setOutDirs($outdir, $errdir)
	{
		$this->outdir = $outdir;
		$this->errdir = $errdir;
		return $this;
	}

	/**
	 * Set debug flags for testing purposes
	 */
	public function setDebugOpts($to_time, $to_write_ticket)
	{
		$this->to_time = (bool)$to_time;
		$this->to_write_ticket = (bool)$to_write_ticket;
		return $this;
	}

	/**
	 * Contract for strategy implementation
	 */
	public function run()
	{
		if ($this->to_time) { $this->prof_flag('Generate purge command.'); }
		$this->generate();
		if ($this->to_time) { $this->prof_flag('Execute purge command.'); }
		$this->execute();
		if ($this->to_time) { $this->prof_flag('Analyze and output purge results.'); }
		$this->results();
		if ($this->to_time) {
			$this->prof_flag('Done.');
			$this->prof_print();
		}
		$this->syserr->commit();
	}

	### ==================== ABSTRACT FUNCTIONS ==================== ###

	abstract protected function generate();
	abstract protected function execute();
	abstract protected function results();

	### ==================== HELPER FUNCTIONS ==================== ###

	/**
	 * purge結果寫入ticket page
	 */
	protected function writeTicket($ticketpage, $numofurls, $result_status, $submission_time)
	{
		$ofp = fopen($ticketpage,'w+');
		if ($ofp) {
			$return = array(
				'originalQueueLength'=>$numofurls,
				'completionTime'=>date("Y-m-d H:i:s"),
				'purgeStatus'=>'Done', //purgeStatus = $result_status?
				'submissionTime'=>$submission_time
			);
			fwrite($ofp, json_encode($return));
			fclose($ofp);
		}
		else {
			$this->syserr->record("無法寫入ticket page: $ticketpage");
			//die('Cannot write into ticket page. Critical error!');
		}
	}

	/**
	 * Return required variables from URL; return empty array if none found
	 * Allows swapping out host
	 * REVIEW Watch out for injection attack via URL
	 */
	protected function touchURL($url, $modifyhost=NULL)
	{
		$result = array();
		$urlparts = parse_url($url);
		if ($urlparts && isset($urlparts['host']) && isset($urlparts['scheme']))
		{
			if (isset($modifyhost))
			{
				$result['host'] = $urlparts['host'];
				$urlparts['host'] = $modifyhost;
				$result['uri'] = $this->unparse_url($urlparts);
			}
			else
			{
				$result['uri'] = $url;
			}

			$result['ishttps'] = (strtolower($urlparts['scheme']) === 'https') ? true : false;
		}
		return $result;
	}

	/**
	 * Used for piecing associative array returned by parse_url() into URL
	 */
	protected function unparse_url($parsed_url) {
		$scheme   = isset($parsed_url['scheme']) ? $parsed_url['scheme'] . '://' : '';
		$host     = isset($parsed_url['host']) ? $parsed_url['host'] : '';
		$port     = isset($parsed_url['port']) ? ':' . $parsed_url['port'] : '';
		$user     = isset($parsed_url['user']) ? $parsed_url['user'] : '';
		$pass     = isset($parsed_url['pass']) ? ':' . $parsed_url['pass']  : '';
		$pass     = ($user || $pass) ? "$pass@" : '';
		$path     = isset($parsed_url['path']) ? $parsed_url['path'] : '';
		$query    = isset($parsed_url['query']) ? '?' . $parsed_url['query'] : '';
		$fragment = isset($parsed_url['fragment']) ? '#' . $parsed_url['fragment'] : '';
		return "$scheme$user$pass$host$port$path$query$fragment";
	}

	/**
	 * To recursively remove directory
	 */
	protected function delTree($dir) {
		$files = array_diff(scandir($dir), array('.','..'));
			foreach ($files as $file) {
				(is_dir("$dir/$file")) ? $this->delTree("$dir/$file") : unlink("$dir/$file");
		}
		return rmdir($dir);
	}

	// Call this at each point of interest, passing a descriptive string
	protected function prof_flag($str)
	{
		global $prof_timing, $prof_names;
		$prof_timing[] = microtime(true);
		$prof_names[] = $str;
	}

	// Call this when you're done and want to see the results
	protected function prof_print()
	{
		global $prof_timing, $prof_names;
		$size = count($prof_timing);
		for($i=0;$i<$size - 1; $i++)
		{
			echo "{$prof_names[$i]}\r\n";
			echo sprintf("\t%f\r\n", $prof_timing[$i+1]-$prof_timing[$i]);
		}
		echo "{$prof_names[$size-1]}\r\n";
	}

}

?>
