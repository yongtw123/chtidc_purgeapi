<?php

use Purge\Log;
use Purge\LogLevel;

require_once 'purge_log.php';

class PurgeAnalysis {

	const OK = 0, FAIL = 1, ERROR = 2;
	private $LogOk, $LogFail, $LogError;
	private $ticketid, $numofhosts, $numofurls, $to_log;
	private $outdir, $errdir;

	public function __construct($ticketid, $urls, $hosts)
	{
		$this->syserr = Log::getInstance(LogLevel::SYSTEM, $ticketid);

		$this->ticketid = $ticketid;
		$this->urls = $urls;
		$this->hosts = $hosts;
		$this->numofurls = count($urls);
		$this->numofhosts = count($hosts);
	}

	/**
	 * Optionally call if responses are stored as files on disk (PSSH)
	 * Attemps to parse files and return data structure for analysis (returns Array)
	 * If $run_immediate TRUE, analysis begins immediately (return String)
	 * If $to_log TRUE, will dump stuff from $errdir
	 * parse() should only do:
	 * 1. check outdirlist errdirlist
	 * 2. check can open
	 * 3. read in each line into data strcuture
	 *
	 * Validation Rules:
	 * + host missing from output (need list of hosts)
	 * + stderr stdout mismatch
	 * + stdout/stderr file open ok? (permission)
	 * + stdout file empty
	 * + url index empty, null, or non-numeric
	 */
	public function parse($stdoutdir, $stderrdir, $run_immediate=false, $to_log=false)
	{
		# Make sure path ends with slash
		$outdir = rtrim($stdoutdir, '/').'/';
		$errdir = rtrim($stderrdir, '/').'/';

		# Scan for stdout/stderr files
		$outfilelist = array_diff(scandir($outdir), array('..', '.')); //remove '.' and '..' dir
		$errfilelist = array_diff(scandir($errdir), array('..', '.'));

		# VALIDATION: hosts 遺漏?
		$diff = array_diff($this->hosts, $outfilelist);
		if (count($diff) > 0) {
			$this->syserr->record("回傳結果hosts遺漏, host連線異常? ".join(',', $diff));
		}

		# VALIDATION: stdout/stderr檔案名稱數量相符?
		$diff = array_unique(array_merge(
			array_diff($outfilelist, $errfilelist),
			array_diff($errfilelist, $outfilelist)
		));
		if (count($diff) > 0) {
			$this->syserr->record("stdout與stderr檔案名稱或數量不符, host連線異常? ".join(',', $diff));
		}

		# Iterate over found stdout files, load all available values
		$temp = array(); //initialize array else if nothing to run then php error
		foreach($outfilelist as $host)
		{
			# VALIDATION: 檔案可正常開啟? (權限問題)
			if (!is_readable($outdir.$host)) {
				$this->syserr->record("無法開啟回傳結果檔案 $outdir$host, 權限問題");
				continue;
			}

			$stdout = file($outdir.$host);

			# VALIDATION: 此host stdout內容不為空? (否則代表host連線有問題, 檢查stderr)
			if ($stdout === '') {
				$this->syserr->record("$host 回傳stdout結果為空");
				continue;
			}

			foreach($stdout as $line)
			{
				$line = trim($line);
				if ($line !== '') { // Note: must NOT use `empty` to test empty string, "0" will return true!!!
					$tokens = array_pad(explode(' ', $line, 2), 2, ''); //tokens[0] url id, tokens[1] http status code, can be "" if malformed

					# VALIDATION: URL index 不存在或非數字?
					if (is_numeric($tokens[0])) { //is_numeric returns FALSE on empty string or null
						$temp[$host][intval($tokens[0])] = intval($tokens[1]);
					}
					else {
						$this->syserr->record("$host 回傳stdout含非法URL index: $tokens[0]");
					}
				}
			}
		}

		if ($to_log) { $this->dump_stderr($errdir); } //just dump stuff from stderrdir
		return ($run_immediate) ? $this->run($temp, $to_log) : $temp;
	}

	/**
	 * Shortcut method of calling all analysis steps in succession
	 *
	 * Algorithm
	 * 1. validate and transpose (host=>urlid, status_code) => (url=>hosts, status_code)
	 * 2. generate the 3 host lists {ok, fail, error} for each URL (URL=>array(3)=>array(hosts))
	 * 3. using host lists, generate status code for each URL (URL=>string)
	 * 4. using status codes, determine batch status
	 */
	public function run($input, $to_log=true)
	{
		$result_status = $this->gen_status(
			$this->gen_urlstatus(
				$urlstatushost = $this->gen_urlstatushost(
					$this->validate($input)
				)
			)
		);

		if ($to_log) {
			$this->logtransform($urlstatushost);
		}

		//this means if files do not exist they will be created
		$this->syserr->commit();

		return $result_status;
	}

	/* Validate, and transpose
	   $obj - Associative array Host - URL index - HTTP code
	   Note: NO ANALYSIS should be done here: separation of responsibilities

	   Validation rules:
	   + stdout missing url id(s) (need list of original urls)
	   + url id out of range
	   + url id missing status code value
	   + url id status code value invalid (not number)
	 */
	public function validate($obj)
	{
		# Prepare transpose data structure, default all null
		# Any url-host value not explicitly changed will be null => stderr will be concatenated
		$url_index_range = range(0, count($this->urls)-1);
		$transpose = array_fill_keys($url_index_range, array_fill_keys($this->hosts, null)); //range() is inclusive

		if (is_null($obj))
		{
			$this->syserr->record("無回傳結果");
			return $transpose;
		}

		foreach($obj as $host=>$url)
		{
			# VALIDATION: URL 遺漏? (curl_multi: 可能因有錯誤跳過)
			$diff = array_diff($url_index_range, array_keys($url));
			if (count($diff) > 0) {
				foreach($diff as $k=>$u)
				{
					$diff[$k] = $this->urls[$u];
				}
				$this->syserr->record("$host 回傳結果URL遺漏 ".join(',', $diff));
			}

			foreach($url as $url_index=>$code)
			{
				# VALIDATION: URL index 超出範圍?
				if ($url_index >= $this->numofurls || $url_index < 0) {
					$this->syserr->record("$host 回傳URL id為無效值: '$url_index' (Max $this->numofurls)");
					continue; //next line
				}

				# VALIDATION: URL STATUS CODE 為無效值?
				if (!is_int($code) || $code < 100 || $code > 599) {
					$codevar = print_r($code, true);
					$this->syserr->record("$host 回傳URL HTTP code為無效值: '$codevar'", $this->urls[$url_index]);
					continue; //next line
				}

				$transpose[$url_index][$host] = $code;
			}
		}
		return $transpose;
	}

	/* Generate host lists {ok, fail, error} for each URL
	   Return data structure (URL - {Ok,Fail,Error} - array(Host))
	 */
	public function gen_urlstatushost($urlhostcode)
	{
		$urlstatushost = array();
		foreach($urlhostcode as $url_index => $hostlist)
		{
			foreach($hostlist as $host=>$code)
			{
				if ($code === 200) {
					$urlstatushost[$url_index][self::OK][] = $host;
				}
				elseif (is_null($code)) {
					$urlstatushost[$url_index][self::ERROR][] = $host;
				}
				else {
					$urlstatushost[$url_index][self::FAIL][$host] = $code;
				}
			}
		}
		return $urlstatushost;
	}

	/* Generate status for each URL
	   Stores result in $this->urlstatus
	 */
	public function gen_urlstatus($urlstatushost)
	{
		$urlstatus = array();
		//logic: if error list > 0, url: error, break; if fail list > 0, url: fail, break; else url: ok
		foreach($urlstatushost as $url_index => $lists)
		{
			if (array_key_exists(self::ERROR, $lists))
			{
				$urlstatus[$url_index] = self::ERROR;
			}
			elseif (array_key_exists(self::FAIL, $lists))
			{
				$urlstatus[$url_index] = self::FAIL;
			}
			elseif (array_key_exists(self::OK, $lists) && count($lists[self::OK]) === $this->numofhosts)
			{
				$urlstatus[$url_index] = self::OK;
			}
			else
			{
				$this->syserr->record("無法判定URL回傳狀態", $this->urls[$url_index]);
			}
		}
		return $urlstatus;
	}

	/* Generate status for entire batch/ticket
	 */
	public function gen_status($urlstatus)
	{
		//logic: if any url error, error, break; if any fail, fail, break; else ok
		$val = array_values($urlstatus);
		if (in_array(self::ERROR, $val))
		{
			return self::ERROR;
		}
		else if (in_array(self::FAIL, $val))
		{
			return self::FAIL;
		}
		else if (in_array(self::OK, $val))
		{
			return self::OK;
		}
		else
		{
			$this->syserr->record("無法判定此ticket回傳狀態: ".join(',', $val));
			return self::ERROR;
		}
	}

	/* Transform host lists to corresponding logs, if required
	 */
	private function logtransform($urlstatushost)
	{
		$log_ok = Log::getInstance(LogLevel::OK, $this->ticketid);
		$log_fail = Log::getInstance(LogLevel::FAIL, $this->ticketid);
		$log_error = Log::getInstance(LogLevel::ERROR, $this->ticketid);

		foreach($urlstatushost as $url_index => $hostlists)
		{
			$url = $this->urls[$url_index];

			//200 Ok
			if (array_key_exists(self::OK, $hostlists)) {
				$log_ok->record(
					(count($hostlists[self::OK]) === $this->numofhosts) ? '<ALL HOSTS OK>' : $hostlists[self::OK],
					$url
				);
			}

			//Error
			//$stderr_to_dump = array_merge($stderr_to_dump, array_flip($hostlists["Error"]));
			if (array_key_exists(self::ERROR, $hostlists)) {
				$log_error->record(
					(count($hostlists[self::ERROR]) === $this->numofhosts) ? '<ALL HOSTS ERR>' : $hostlists[self::ERROR],
					$url
				);
			}

			//non-200 Fail
			if (array_key_exists(self::FAIL, $hostlists)) {
				$codes = array_unique( array_values($hostlists[self::FAIL]) );
				if (count($codes) === 1 && count($hostlists[self::FAIL]) === $this->numofhosts) {
					$failtemp = "<ALL HOSTS $codes[0]>";
				}
				else {
					foreach($hostlists[self::FAIL] as $host=>$code)
					{
						$failtemp[] = "$host=$code";
					}
				}
				$log_fail->record($failtemp, $url);
				unset($failtemp); //remember to clear the variable for next iteration!
			}
		}

		$log_ok->commit();
		$log_error->commit();
		$log_fail->commit();
		//NOTE If these logs point to same file, be careful of commit order, else log will not be of chronological order.
	}

	private function dump_stderr($errdir)
	{
		foreach($this->hosts as $host)
		{
			if (!file_exists($errdir.$host)) {
				$this->syserr->record("找不到回傳結果檔案 $errdir$host");
				continue;
			}

			if (!is_readable($errdir.$host)) {
				$this->syserr->record("無法開啟回傳結果檔案 $errdir$host, 權限問題");
				continue;
			}

			$stderr = file($errdir.$host);
			$stderr = array_filter($stderr, 'trim'); //filter out any empty lines

			if (!empty($stderr)) {
				$this->syserr->write("=== stderr FROM $host ===".PHP_EOL);
				$this->syserr->write($stderr);
				$this->syserr->write(PHP_EOL);
			}
		}
	}
}
?>
