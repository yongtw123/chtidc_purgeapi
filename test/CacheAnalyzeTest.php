<?php

require 'purge_cacheanalyze.php';

class CacheAnalyzeTest extends \PHPUnit_Framework_TestCase
{
	private static $urls, $hosts, $tolog;

	public static function setUpBeforeClass()
	{
		$urlsasjson = file('test/urls.json');
		self::$urls = json_decode(join($urlsasjson, ''));

		$hostsasjson = file('test/hosts.json');
		self::$hosts = json_decode(join($hostsasjson, ''));

		//self::$tolog = false;
		self::$tolog = true;
	}

	//case 00: all host return, all url return, result homogeneous, all url 200 Ok
	public function testScenario00()
	{
		$pa = new PurgeAnalysis('test000', array_slice(self::$urls, 0, 3), array_slice(self::$hosts, 0, 3));
		$input = $pa->parse('test/cases/00/stdout/', 'test/cases/00/stderr/');
		$transpose = $pa->validate($input);
		$hostlists = $pa->gen_urlstatushost($transpose);

		$urlstatus = $pa->gen_urlstatus($hostlists);
		$this->assertCount(3, $urlstatus);
		$this->assertArraySubset(array_keys($urlstatus), range(0, count(self::$urls)-1));

		$status = $pa->gen_status($urlstatus);
		$this->assertEquals($status, PurgeAnalysis::OK);
	}

	//case 01: all host return, all url return, result homogeneous, all url non-200 Fail
	public function testScenario01()
	{
		$pa = new PurgeAnalysis('test001', array_slice(self::$urls, 0, 5), array_slice(self::$hosts, 0, 7));
		$input = $pa->parse('test/cases/01/stdout/', 'test/cases/01/stderr/');
		$transpose = $pa->validate($input);
		$hostlists = $pa->gen_urlstatushost($transpose);

		$urlstatus = $pa->gen_urlstatus($hostlists);
		$this->assertCount(5, $urlstatus);
		$this->assertArraySubset(array_keys($urlstatus), range(0, count(self::$urls)-1));

		$status = $pa->gen_status($urlstatus);
        $this->assertEquals($status, PurgeAnalysis::FAIL);

		//TODO How to test if output log is correct??
	}

	//case 02: all host return, all url return, result homogeneous, all url Error
    public function testScenario02()
    {
        $pa = new PurgeAnalysis('test002', array_slice(self::$urls, 0, 3), array_slice(self::$hosts, 0, 3));
		$input = $pa->parse('test/cases/02/stdout/', 'test/cases/02/stderr/');
		$transpose = $pa->validate($input);
		$hostlists = $pa->gen_urlstatushost($transpose);

		$urlstatus = $pa->gen_urlstatus($hostlists);
		$this->assertCount(3, $urlstatus);
		$this->assertArraySubset(array_keys($urlstatus), range(0, count(self::$urls)-1));

		$status = $pa->gen_status($urlstatus);
        $this->assertEquals($status, PurgeAnalysis::ERROR);
    }

	//case 03: all host return, all url return, result homogeneous, mix of 200, non-200, error
    public function testScenario03()
    {
        $pa = new PurgeAnalysis('test003', array_slice(self::$urls, 0, 9), array_slice(self::$hosts, 0, 5));
		$input = $pa->parse('test/cases/03/stdout/', 'test/cases/03/stderr/');
		$transpose = $pa->validate($input);
		$hostlists = $pa->gen_urlstatushost($transpose);

		//no point in testing urlhostsc => all hosts are the same anyways

		$urlstatus = $pa->gen_urlstatus($hostlists);
		$this->assertCount(9, $urlstatus);
		$this->assertArraySubset(array_keys($urlstatus), range(0, count(self::$urls)-1));
		$this->assertEquals($urlstatus[0], PurgeAnalysis::FAIL);
        $this->assertEquals($urlstatus[5], PurgeAnalysis::ERROR);
        $this->assertEquals($urlstatus[7], PurgeAnalysis::OK);

		$status = $pa->gen_status($urlstatus);
        $this->assertEquals($status, PurgeAnalysis::ERROR);
    }

	//case 04: all host return, all url return, result heterogeneous, mix of 200, non-200, error
    public function testScenario04()
    {
        $pa = new PurgeAnalysis('test004', array_slice(self::$urls, 0, 5), array_slice(self::$hosts, 0, 7));
		$input = $pa->parse('test/cases/04/stdout/', 'test/cases/04/stderr/');

		$transpose = $pa->validate($input);
		$this->assertCount(5, $transpose);
		$this->assertEquals($transpose[0]['testhost01'], '404');
		$this->assertEquals($transpose[1]['testhost05'], '200');
		$this->assertEquals($transpose[1]['testhost06'], null);
		$this->assertEquals($transpose[2]['testhost02'], null);
		$this->assertEquals($transpose[3]['testhost04'], '200');
		$this->assertEquals($transpose[4]['testhost07'], '200');

		$hostlists = $pa->gen_urlstatushost($transpose);
		$urlstatus = $pa->gen_urlstatus($hostlists);
		$this->assertCount(5, $urlstatus);
		$this->assertArraySubset(array_keys($urlstatus), range(0, count(self::$urls)-1));
        $this->assertEquals($urlstatus[0], PurgeAnalysis::FAIL);
        $this->assertEquals($urlstatus[1], PurgeAnalysis::ERROR);
        $this->assertEquals($urlstatus[4], PurgeAnalysis::OK);

		$status = $pa->gen_status($urlstatus);
        $this->assertEquals($status, PurgeAnalysis::ERROR);
    }

	//case 05: all host return, some url missing (may be skipped due to malformed), result heterogeneous, mix
	//malformed (no space after url id, empty lines, extra spaces)
	public function testScenario05()
	{
		$pa = new PurgeAnalysis('test005', array_slice(self::$urls, 0, 4), array_slice(self::$hosts, 0, 2));
		$input = $pa->parse('test/cases/05/stdout/', 'test/cases/05/stderr/');

		$transpose = $pa->validate($input);
		$this->assertCount(4, $transpose);
		$this->assertEquals($transpose[0]['testhost01'], null);
		$this->assertEquals($transpose[1]['testhost01'], '200');
		$this->assertEquals($transpose[2]['testhost01'], null);
		$this->assertEquals($transpose[0]['testhost02'], '200');
		$this->assertEquals($transpose[1]['testhost02'], '200');
		$this->assertEquals($transpose[2]['testhost02'], null);

		$hostlists = $pa->gen_urlstatushost($transpose);
		$urlstatus = $pa->gen_urlstatus($hostlists);
		$this->assertCount(4, $urlstatus);
		$this->assertEquals($urlstatus[0], PurgeAnalysis::ERROR);
        $this->assertEquals($urlstatus[1], PurgeAnalysis::OK);
        $this->assertEquals($urlstatus[2], PurgeAnalysis::ERROR);
        $this->assertEquals($urlstatus[3], PurgeAnalysis::FAIL);

		$status = $pa->gen_status($urlstatus);
		$this->assertEquals($status, PurgeAnalysis::ERROR);
	}

	//case 06: some host missing, all url return, result heterogeneous, mix
	public function testScenario06()
	{
		$pa = new PurgeAnalysis('test006', array_slice(self::$urls, 0, 2), array_slice(self::$hosts, 0, 4));
		$input = $pa->parse('test/cases/06/stdout/', 'test/cases/06/stderr/');

		$transpose = $pa->validate($input);
		$this->assertCount(2, $transpose);
		$this->assertEquals($transpose[0]['testhost01'], '404');
		$this->assertEquals($transpose[1]['testhost01'], '200');
		$this->assertEquals($transpose[0]['testhost02'], null);
		$this->assertEquals($transpose[1]['testhost02'], null);
		$this->assertEquals($transpose[0]['testhost03'], '404');
		$this->assertEquals($transpose[1]['testhost04'], null);

		$hostlists = $pa->gen_urlstatushost($transpose);
		$urlstatus = $pa->gen_urlstatus($hostlists);
		$this->assertCount(2, $urlstatus);
		$this->assertEquals($urlstatus[0], PurgeAnalysis::ERROR);
        $this->assertEquals($urlstatus[1], PurgeAnalysis::ERROR);

		$status = $pa->gen_status($urlstatus);
		$this->assertEquals($status, PurgeAnalysis::ERROR);
	}

	//case 07: some host missing, some url missing, result heterogeneous, mix
	public function testScenario07()
	{
		$pa = new PurgeAnalysis('test007', array_slice(self::$urls, 0, 4), array_slice(self::$hosts, 0, 4));
		$input = $pa->parse('test/cases/07/stdout/', 'test/cases/07/stderr/');

		$transpose = $pa->validate($input);
		$this->assertCount(4, $transpose);
		$this->assertEquals($transpose[0]['testhost01'], '404');
		$this->assertEquals($transpose[2]['testhost01'], null);
		$this->assertEquals($transpose[0]['testhost02'], '200');
		$this->assertEquals($transpose[3]['testhost02'], null);
		$this->assertEquals($transpose[0]['testhost03'], null);
		$this->assertEquals($transpose[0]['testhost04'], '405');
		$this->assertEquals($transpose[1]['testhost04'], null);

		$hostlists = $pa->gen_urlstatushost($transpose);
		$urlstatus = $pa->gen_urlstatus($hostlists);
		$this->assertCount(4, $urlstatus);
		$this->assertEquals(
			join('', array_unique(array_values($urlstatus))),
			PurgeAnalysis::ERROR
		);

		$status = $pa->gen_status($urlstatus);
		$this->assertEquals($status, PurgeAnalysis::ERROR);
	}

}

?>
