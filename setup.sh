#!/bin/bash

DEST="/var/www/html/purgeapi"
if [ ! -d $DEST ]; then
  mkdir $DEST
fi

BASEDIR=$(dirname "$0")
cp $BASEDIR/purge*.php $DEST
cp $BASEDIR/.htaccess $DEST
cp $BASEDIR/config.php $DEST

cd $DEST
ln -s purge.php purgerp06.php
ln -s purgegui.php purgerp_gui.php

cd $(dirname $DEST)
chown -R apache $(basename $DEST)
